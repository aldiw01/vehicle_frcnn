clear all
load('results\testResultsFilter5.mat')
color = 'mrgbk';
line = ["-" "--" ":" "-."];
mark = 'o+*.xsd^v><ph';
k=1;
f = figure;

x = [3023160453 3288974469 3687695493 3953509509 865191045 1173472389 ...
    1439286405 1838007429 2103821445 760391301 775362693 1083644037 ...
    1349458053 1748179077 2013993093];

for i=1:15
    c(i) = categorical(replace(string(cell2mat(result.filter(i*3))),'_','-'));
end


bar(c,x);
ylim([0 4.5e9])
% set(gca,'YScale','log')
% plot(x,y,mark(mod(i,13)+1),'MarkerIndices',1:length(x),...
%     'LineStyle',line(mod(i,4)+1),...
%     'Color',color(mod(i,5)+1),...
%     'LineWidth',2)

text(1:length(x),x,num2str(x'),'vert','bottom','horiz','center'); 

xlabel('Skema Filter')
ylabel('Operasi')
% axis([10 130 11.4 13])
% legend({'Operasi'},'Location','northeast');
% title(sprintf('Mean Average Precision Comparison'))

%% Save Figure as pdf
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3)-3, pos(4)])
print(f,'OperasiFilter','-dpdf','-r0')
