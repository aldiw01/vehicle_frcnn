x = pwd;
cd (strcat(x,'\results\padding\'))
net = struct2cell(dir);
net = net(1,3:end,:);
cd (x)

%% Load Dataset
t1 = load('dataset/testV1.mat');
t2 = load('dataset/testV2.mat');
t3 = load('dataset/testV3.mat');
t4 = load('dataset/testV4.mat');
t5 = load('dataset/testV5.mat');
t6 = load('dataset/testImg.mat');

testDataset = [...
%         t1.gTruth
%         t2.gTruth
%         t3.gTruth
%         t4.gTruth
%         t5.gTruth
    t6.gTruth
    ];

for n=1:1
    cell2mat(net(n))
    load(strcat(pwd,'\results\padding\',cell2mat(net(n))))
    %%
    % Run detector on each image in the test set and collect results.
    resultsStruct = struct([]);
    for i = 1:height(testDataset)

        % Read the image.
        I = imread(testDataset.imageFilename{i});
        % Run the detector.
        tic
        [bboxes, scores, labels] = detect(detector, I);
        time(i) = toc;
        % Collect the results.
        resultsStruct(i).Boxes = bboxes;
        resultsStruct(i).Scores = scores;
        resultsStruct(i).Labels = labels;
        if(mod(i,1000)==0)
            x = num2str([i height(testDataset) i/height(testDataset)*100])
        end
    end

    % Convert the results into a table.
    results = struct2table(resultsStruct);
    %% Extract expected bounding box locations from test data.
    expectedResults = testDataset(:, 2:end);

    % Evaluate the object detector using Average Precision metric.
    [ap, recall, precision] = evaluateDetectionPrecision(results, expectedResults);
    %% 
    % The precision/recall (PR) curve highlights how precise a detector is at 
    % varying levels of recall. Ideally, the precision would be 1 at all recall levels. 
    % In this example, the average precision is ~0.6. The use of additional layers 
    % in the network can help improve the average precision, but might require additional 
    % training data and longer training time.
    %%
    % Plot precision/recall curve
%     for obj = 1:size(ap(n,:),1)
%         figure
%         plot(cell2mat(recall(obj)),cell2mat(precision(obj)))
%         xlabel('Recall')
%         ylabel('Precision')
%         grid on
%         title(sprintf('Average Precision %s = %.1f', ...
%             cell2mat(expectedResults.Properties.VariableNames(obj)), ap(n,obj)*100))
%     end
    result.net = string(cell2mat(net(n)));
    result.ap = ap;
    result.recall = recall;
    result.precision = precision;
    save(sprintf('results/testResultsFilt_%s.mat',cell2mat(net(n))),'result');
end
