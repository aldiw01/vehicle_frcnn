ep = [10 50 100];
x = [12.5031523102587 12.6563089547115 12.3273336155055];
y = [12.64 12.80 12.77];
z = [12.77 12.45 12.32];

f = figure;
plot(ep,x,'-o','MarkerIndices',1:length(x),...
    'LineStyle','--',...
    'Color','b',...
    'LineWidth',1.5)

hold on
plot(ep,y,'-d','MarkerIndices',1:length(x),...
    'LineStyle','-',...
    'Color','r',...
    'LineWidth',1.5)

hold on
plot(ep,z,'-v','MarkerIndices',1:length(x),...
    'LineStyle','-.',...
    'Color','k',...
    'LineWidth',1.5)

xlabel('Epoch')
ylabel('FPS')
grid on
axis([10 100 12 13])
legend({'Full-training','11-3-3-3-3','Transfer Learning'},'Location','southeast');
% title(sprintf('Mean Average Precision Comparison'))

% Save Figure as pdf
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(f,'mapResults','-dpdf','-r0')
