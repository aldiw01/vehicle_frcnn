load('results\testResultsFilter5.mat')
color = 'mrgbk';
line = ["-" "--" ":" "-."];
mark = 'o+*.xsd^v><ph';
k=1;
f = figure;

x = [60661125 62233989 64593285 66166149 57908613 58301829 59874693 ...
    62233989 63806853 57876357 57880965 58274181 59847045 62206341 63779205];

for i=1:15
    c(i) = categorical(replace(string(cell2mat(result.filter(i*3))),'_','-'));
end

bar(c,x);
ylim([5.5e7 7e7])
set(gca,'YScale','log')
% plot(x,y,mark(mod(i,13)+1),'MarkerIndices',1:length(x),...
%     'LineStyle',line(mod(i,4)+1),...
%     'Color',color(mod(i,5)+1),...
%     'LineWidth',2)

text(1:length(x),x,num2str(x'),'vert','bottom','horiz','center'); 

xlabel('Skema Filter')
ylabel('Learnable Parameter')
% axis([10 130 11.4 13])
% legend({'Hyperparameter'},'Location','northeast');
% title(sprintf('Mean Average Precision Comparison'))

%% Save Figure as pdf
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3)-3, pos(4)])
print(f,'ParameterFilter','-dpdf','-r0')

