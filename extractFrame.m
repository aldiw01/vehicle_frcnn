
%% Load Video
input = VideoReader('dataset/Video/4.mp4');
frame=1;
while hasFrame(input)
    %% Read Frame for Video
    img = readFrame(input);
    imwrite(img,sprintf('dataset/Test/Video/Video4/img%04d.jpg',frame));
    frame = frame+1;
end