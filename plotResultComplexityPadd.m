clear all
color = 'mrgbk';
line = ["-" "--" ":" "-."];
mark = 'o+*.xsd^v><ph';
k=1;
f = figure;

x = [1131838853 1173472389 610725253];

c = categorical({'Alexnet','Same-Padding','Valid-Padding'});

bar(c,x);
ylim([0 12.5e8])
% set(gca,'YScale','log')
% plot(x,y,mark(mod(i,13)+1),'MarkerIndices',1:length(x),...
%     'LineStyle',line(mod(i,4)+1),...
%     'Color',color(mod(i,5)+1),...
%     'LineWidth',2)

text(1:length(x),x,num2str(x'),'vert','bottom','horiz','center'); 

% xlabel('Skema Padding')
ylabel('Operasi')
% axis([10 130 11.4 13])
% legend({'Operasi'},'Location','northeast');
% title(sprintf('Mean Average Precision Comparison'))

%% Save Figure as pdf
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(f,'OperasiPadding','-dpdf','-r0')
