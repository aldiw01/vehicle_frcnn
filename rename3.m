close all;
clc;
y=561;
for i=110685:117008
    x = num2str(i);
    for j=1:8-length(x)
        x = ['0' x];
    end
    if(exist(sprintf('dataset/Train/%s.jpg',x),'file'))
        img = imread(sprintf('dataset/Train/%s.jpg',x));
        s = size(img);
        if(min(s(1:2))<200)
            img = imresize(img,2);
        elseif(min(s(1:2))<240)
            img = imresize(img,1.5);
%         elseif(max(s)>1000)
%             img = imresize(img,1/2);
        end
        delete(sprintf('dataset/Train/%s.jpg',x));
        imwrite(img,sprintf('dataset/Train/%d.jpg',y));
        y=y+1;
    end
end