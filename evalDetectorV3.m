x = pwd;
cd (strcat(x,'\results\padding\'))
net = struct2cell(dir);
net = net(1,3:end,:);
cd (x)
    %% Load Dataset
    t1 = load('dataset/testV1.mat');
    t2 = load('dataset/testV2.mat');
    t3 = load('dataset/testV3.mat');
    t4 = load('dataset/testV4.mat');
    t5 = load('dataset/testV5.mat');
    t6 = load('dataset/testImg.mat');
    
    testDataset = [...
        t1.gTruth
        t2.gTruth
        t3.gTruth
        t4.gTruth
        t5.gTruth
        t6.gTruth
        ];
for n=1:length(net)
    cell2mat(net(n))
    load(strcat(pwd,'\results\padding\',cell2mat(net(n))))
    %%
    % Run detector on each image in the test set and collect results.
    resultsStruct = struct([]);
    for i = 1:height(testDataset)

        % Read the image.
        I = imread(testDataset.imageFilename{i});
        % Run the detector.
        tic
        [bboxes, scores, labels] = detect(detector, I);
        time2(i) = toc;
        [x,~] = size(bboxes);
        detectedImg = I;
        %%
        % Display detection results.
        for j=1:x
            if scores(j) > 0.0
                if labels(j) == 'Car'
                    detectedImg = insertObjectAnnotation(detectedImg,...
                        'Rectangle',bboxes(j,:),sprintf('%s - %.1f',...
                        labels(j),scores(j)),'LineWidth',3,...
                        'Color','cyan','FontSize',15,...
                        'Font','LucidaSansDemiBold');
                elseif labels(j) == 'Motorcycle'
                    detectedImg = insertObjectAnnotation(detectedImg,...
                        'Rectangle',bboxes(j,:),sprintf('%s - %.1f',...
                        labels(j),scores(j)),'LineWidth',3,...
                        'Color','magenta','FontSize',15,...
                        'Font','LucidaSansDemiBold');
                elseif labels(j) == 'Bus'
                    detectedImg = insertObjectAnnotation(detectedImg,...
                        'Rectangle',bboxes(j,:),sprintf('%s - %.1f',...
                        labels(j),scores(j)),'LineWidth',3,...
                        'Color','yellow','FontSize',15,...
                        'Font','LucidaSansDemiBold');
                elseif labels(j) == 'Truck'
                    detectedImg = insertObjectAnnotation(detectedImg,...
                        'Rectangle',bboxes(j,:),sprintf('%s - %.1f',...
                        labels(j),scores(j)),'LineWidth',3,...
                        'Color','green','FontSize',15,...
                        'Font','LucidaSansDemiBold');
                end
            end
        end
        imshow(detectedImg)
        drawnow;
        time(i) = toc;
        % Collect the results.
        resultsStruct(i).Boxes = bboxes;
        resultsStruct(i).Scores = scores;
        resultsStruct(i).Labels = labels;
        if(mod(i,1000)==0)
            x = num2str([i height(testDataset) i/height(testDataset)*100])
        end
    end

    % Convert the results into a table.
    results = struct2table(resultsStruct);
    %% Extract expected bounding box locations from test data.
    expectedResults = testDataset(:, 2:end);

    % Evaluate the object detector using Average Precision metric.
    [ap(n,:), recall, precision] = evaluateDetectionPrecision(results, expectedResults);
    
    %% 
    result(n).net = net(n);
    result(n).filter = extractBetween(net(n),"Filt_","_Ep");
    result(n).epoch = str2double(extractBetween(net(n),"_Ep","_64"));
    result(n).ap = ap(n,:);
    result(n).map = mean(result(n).ap);
    result(n).time = mean(time(2:end));
    result(n).fps = 1/result(n).time;
    result(n).npad = extractBetween(net(n),"_64",".mat");
end
result = struct2table(result);
save('results/testResultsFilter7.mat','result');