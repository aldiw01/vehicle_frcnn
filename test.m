
%% Load Video
% input = VideoReader('visiontraffic.avi');
input = VideoReader('dataset/Video/5.mp4');

%% Write Detection Result Video
% v = VideoWriter('results/5.mp4','MPEG-4');
% v.FrameRate = 10;
% open(v);

frame=1; t=1; time=0;
while hasFrame(input)
    %% Read Frame for Video
    img = readFrame(input);
    if(mod(frame,2)==0)
        %%
        % Run detector.
%         img = imresize(img,.75);
        tic
        [bbox, score, label] = detect(detector, img);
        [x,~] = size(bbox);
        detectedImg = img;
        %%
        % Display detection results.
        for i=1:x
            if score(i) > 0.0
                if label(i) == 'Car'
                    detectedImg = insertObjectAnnotation(detectedImg,...
                        'Rectangle',bbox(i,:),sprintf('%s - %.1f',...
                        label(i),score(i)),'LineWidth',3,...
                        'Color','cyan','FontSize',15,...
                        'Font','LucidaSansDemiBold');
                elseif label(i) == 'Motorcycle'
                    detectedImg = insertObjectAnnotation(detectedImg,...
                        'Rectangle',bbox(i,:),sprintf('%s - %.1f',...
                        label(i),score(i)),'LineWidth',3,...
                        'Color','magenta','FontSize',15,...
                        'Font','LucidaSansDemiBold');
                elseif label(i) == 'Bus'
                    detectedImg = insertObjectAnnotation(detectedImg,...
                        'Rectangle',bbox(i,:),sprintf('%s - %.1f',...
                        label(i),score(i)),'LineWidth',3,...
                        'Color','yellow','FontSize',15,...
                        'Font','LucidaSansDemiBold');
                elseif label(i) == 'Truck'
                    detectedImg = insertObjectAnnotation(detectedImg,...
                        'Rectangle',bbox(i,:),sprintf('%s - %.1f',...
                        label(i),score(i)),'LineWidth',3,...
                        'Color','green','FontSize',15,...
                        'Font','LucidaSansDemiBold');
                end
            end
        end
        imshow(detectedImg)
        drawnow;
        time(t) = toc;
        t = t+1;
%         writeVideo(v,detectedImg);
    end
    frame = frame+1;
end
% close(v);
