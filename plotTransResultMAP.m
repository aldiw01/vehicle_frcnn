ep = [10 50 100];
x = [53.11 72.87 74.51];
y = [55.9 76.2 75.7];
z = [73.8 76.0 75.1];

f = figure;
plot(ep,x,'-o','MarkerIndices',1:length(x),...
    'LineStyle','--',...
    'Color','b',...
    'LineWidth',1.5)

hold on
plot(ep,y,'-d','MarkerIndices',1:length(x),...
    'LineStyle','-',...
    'Color','r',...
    'LineWidth',1.5)

hold on
plot(ep,z,'-v','MarkerIndices',1:length(x),...
    'LineStyle','-.',...
    'Color','k',...
    'LineWidth',1.5)

xlabel('Epoch')
ylabel('mAP (%)')
grid on
axis([10 100 50 80])
legend({'Full-training','11-3-3-3-3','Transfer Learning'},'Location','southeast');
% title(sprintf('Mean Average Precision Comparison'))

% Save Figure as pdf
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(f,'mapResults','-dpdf','-r0')
