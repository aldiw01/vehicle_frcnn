%% Train Faster R-CNN Vehicle Detector

%% Preparing data.
load('dataset/train.mat');
gTruth.imageFilename = strcat(pwd,'\',gTruth.imageFilename);
% trainingData = gTruth;

% t1 = load('dataset/testV1.mat');
% t2 = load('dataset/testV2.mat');
% t3 = load('dataset/testV3.mat');
% t4 = load('dataset/testV4.mat');
% t5 = load('dataset/testV5.mat');
% t6 = load('dataset/testImg.mat');

trainingData = [...
    gTruth
%     t1.gTruth
%     t2.gTruth
%     t3.gTruth
%     t4.gTruth
%     t5.gTruth
%     t6.gTruth
    ];

net = alexnet;
filter = [...
        11	11	5	5	5
        11	11	5	5	3
        11	11	5	3	3
        11	11	3	3	3
        11	5	5	5	5
        11	5	5	5	3
        11	5	5	3	3
        11	5	3	3	3
        11	3	3	3	3
        5	5	5	5	5
        5	5	5	5	3
        5	5	5	3	3
        5	5	3	3	3
        5	3	3	3	3
        3	3	3	3	3
    ];

%% Commencing Faster R-CNN.
for f = 8:8
    %% Setup network layers.
%     if(f==8) 
%         continue;
%     end
    layers = [...
        net.Layers(1)
        convolution2dLayer(filter(f,1), 96, 'Stride', 4, 'Padding', 'same', 'BiasLearnRateFactor', 2, 'NumChannels', 3)
        net.Layers(3:5)
        convolution2dLayer(filter(f,2), 256, 'Stride', 1, 'Padding', 'same', 'BiasLearnRateFactor', 2, 'NumChannels', 96)
        net.Layers(7:9)
        convolution2dLayer(filter(f,3), 384, 'Stride', 1, 'Padding', 'same', 'BiasLearnRateFactor', 2, 'NumChannels', 256)
        net.Layers(11)
        convolution2dLayer(filter(f,4), 384, 'Stride', 1, 'Padding', 'same', 'BiasLearnRateFactor', 2, 'NumChannels', 384)
        net.Layers(13)
        convolution2dLayer(filter(f,5), 256, 'Stride', 1, 'Padding', 'same', 'BiasLearnRateFactor', 2, 'NumChannels', 384)
        net.Layers(15:16)
        fullyConnectedLayer(4096, 'BiasLearnRateFactor', 2)
        net.Layers(18:19)
        fullyConnectedLayer(4096, 'BiasLearnRateFactor', 2)
        net.Layers(21:22)
        fullyConnectedLayer(5, 'BiasLearnRateFactor', 2)
        softmaxLayer
        classificationLayer
        ]
    
    %% Configure training options.
    epoch = [100];
    for i = 1:length(epoch)
        diary(sprintf('logs/LogsPadd_%d_%d_%d_%d_%d_Ep%d_NP.txt', ...
            filter(f,1),filter(f,2),filter(f,3),filter(f,4),filter(f,5),epoch(i)));
        trainOption = trainingOptions('sgdm', ...
            'MaxEpochs', epoch(i), ...
            'InitialLearnRate', 0.001, ...
            'MiniBatchSize',64, ...
            'LearnRateSchedule','piecewise', ...
            'LearnRateDropPeriod',round(epoch(i)*0.6), ...
            'LearnRateDropFactor',0.1, ...
            'VerboseFrequency', 100)
        
        %% Train detector.
        try
            tic
            detector = trainFasterRCNNObjectDetector(trainingData, layers, trainOption, ...
                    'NegativeOverlapRange', [0 0.4], ...
                    'PositiveOverlapRange', [0.6 1], ...
                    'BoxPyramidScale', 1.2)
            toc
            
            % Shows layers
            detector.Network.Layers

            diary off;
            save(sprintf('results/padding/FRCNNPadd_%d_%d_%d_%d_%d_Ep%d_%d_NP.mat', ...
                filter(f,1),filter(f,2),filter(f,3),filter(f,4),filter(f,5),epoch(i),trainOption.MiniBatchSize), 'detector');
        catch ME
%             system('shutdown -s -t 60');
            rethrow(ME)
            diary('Logs.txt');
        end
    end
end
system('shutdown -s -t 60');