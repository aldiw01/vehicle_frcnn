%% Load Dataset
load('dataset/testImg.mat');
frame=1; t=1; time=0;
f = figure;

for i=2:31
    %% Read Frame for Video
    tic
    img = imread(cell2mat(gTruth.imageFilename(i)));
    if(mod(frame,1)==0)
        truth = img;
        bbox = cell2mat(gTruth.Car(i));
        %%
        % Display detection results.
        for j=1:size(bbox,1)
            truth = insertObjectAnnotation(truth, 'Rectangle', ...
                bbox(j,:), sprintf('%s', 'Car'),'LineWidth',3,...
                'Color','cyan','FontSize',15);
        end
        %%%%%%%%%%%
        bbox = cell2mat(gTruth.Motorcycle(i));
        %%
        % Display detection results.
        for j=1:size(bbox,1)
            truth = insertObjectAnnotation(truth, 'Rectangle', ...
                bbox(j,:), sprintf('%s', 'Motorcycle'),'LineWidth',3,...
                'Color','magenta','FontSize',15);
        end
        %%%%%%%%%%%
        bbox = cell2mat(gTruth.Bus(i));
        %%
        % Display detection results.
        for j=1:size(bbox,1)
            truth = insertObjectAnnotation(truth, 'Rectangle', ...
                bbox(j,:), sprintf('%s', 'Bus'),'LineWidth',3,...
                'Color','yellow','FontSize',15);
        end
        %%%%%%%%%%%
        bbox = cell2mat(gTruth.Truck(i));
        %%
        % Display detection results.
        for j=1:size(bbox,1)
            truth = insertObjectAnnotation(truth, 'Rectangle', ...
                bbox(j,:), sprintf('%s', 'Truck'),'LineWidth',3,...
                'Color','green','FontSize',15);
        end
        %%%%%%%%%%%
        imshow(truth)
        title(sprintf('%d / %d',frame, height(gTruth)));
        drawnow;
        time(t) = toc;
        t = t+1;
        pause(1);
    end
    
    imwrite(truth,sprintf('results/figures/dataset/test/%d.png',frame));
    
    % Save Figure as pdf
%     set(f,'Units','Inches');
%     pos = get(f,'Position');
%     set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
%     print(f,num2str(frame),'-dpng','-r0')
    
    frame = frame+1;
end