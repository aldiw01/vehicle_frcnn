%% Train Faster R-CNN Vehicle Detector

%% Preparing data.
load('dataset/train.mat');
gTruth.imageFilename = strcat(pwd,'\',gTruth.imageFilename);
trainingData = gTruth;
net = alexnet;

%% Commencing Faster R-CNN.
for stride = 2:2
    %% Setup network layers.
    
    layers = [...
        net.Layers(1)
        convolution2dLayer(11, 96, 'Stride', stride, 'Padding', 0, 'BiasLearnRateFactor', 2, 'NumChannels', 3)
        net.Layers(3:5)
        convolution2dLayer(5, 256, 'Stride', stride, 'Padding', 2, 'BiasLearnRateFactor', 2, 'NumChannels', 96)
        net.Layers(7:9)
        convolution2dLayer(3, 384, 'Stride', stride, 'Padding', 1, 'BiasLearnRateFactor', 2, 'NumChannels', 256)
        net.Layers(11)
        convolution2dLayer(3, 384, 'Stride', stride, 'Padding', 2, 'BiasLearnRateFactor', 2, 'NumChannels', 384)
        net.Layers(13)
        convolution2dLayer(3, 256, 'Stride', stride, 'Padding', 1, 'BiasLearnRateFactor', 2, 'NumChannels', 384)
        net.Layers(15:16)
        fullyConnectedLayer(4096, 'BiasLearnRateFactor', 2)
        net.Layers(18:19)
        fullyConnectedLayer(4096, 'BiasLearnRateFactor', 2)
        net.Layers(21:22)
        fullyConnectedLayer(5, 'BiasLearnRateFactor', 2)
        softmaxLayer
        classificationLayer
        ]
    
    %% Configure training options.
    epoch = [10 50 100];
    for i = 1:length(epoch)
        trainOption = trainingOptions('sgdm', ...
            'MaxEpochs', epoch(i), ...
            'InitialLearnRate', 0.01, ...
            'CheckpointPath', tempdir);

        %% Train detector.
        try
            tic
            detector = trainFasterRCNNObjectDetector(trainingData, layers, trainOption, ...
                    'NegativeOverlapRange', [0 0.4], ...
                    'PositiveOverlapRange', [0.6 1], ...
                    'BoxPyramidScale', 1.2)
            toc
            save(sprintf('results/stride/FRCNNStr%dEp%d.mat',stride,epoch(i)), 'detector');
        catch ME
%             system('shutdown -s -t 60');
            rethrow(ME)
%             diary('Logs.txt');
        end
    end
end