
trainOption = 

  <a href="matlab:helpPopup nnet.cnn.TrainingOptionsSGDM" style="font-weight:bold">TrainingOptionsSGDM</a> with properties:

                     Momentum: 0.9000
             InitialLearnRate: 1.0000e-03
    LearnRateScheduleSettings: [1�1 struct]
             L2Regularization: 1.0000e-04
      GradientThresholdMethod: 'l2norm'
            GradientThreshold: Inf
                    MaxEpochs: 10
                MiniBatchSize: 64
                      Verbose: 1
             VerboseFrequency: 100
               ValidationData: []
          ValidationFrequency: 50
           ValidationPatience: 5
                      Shuffle: 'once'
               CheckpointPath: ''
         ExecutionEnvironment: 'auto'
                   WorkerLoad: []
                    OutputFcn: []
                        Plots: 'none'
               SequenceLength: 'longest'
         SequencePaddingValue: 0

*************************************************************************
Training a Faster R-CNN Object Detector for the following object classes:

* Car
* Motorcycle
* Bus
* Truck

Step 1 of 4: Training a Region Proposal Network (RPN).
Training on single GPU.
|========================================================================================|
|  Epoch  |  Iteration  |  Time Elapsed  |  Mini-batch  |  Mini-batch  |  Base Learning  |
|         |             |   (hh:mm:ss)   |   Accuracy   |     RMSE     |      Rate       |
|========================================================================================|
|       1 |           1 |       00:00:00 |       42.19% |         0.87 |          0.0010 |
|       1 |         100 |       00:00:08 |       89.06% |         1.12 |          0.0010 |
|       1 |         200 |       00:00:18 |       89.06% |         0.70 |          0.0010 |
|       1 |         300 |       00:00:26 |       90.63% |         1.01 |          0.0010 |
|       1 |         400 |       00:00:35 |       98.44% |         0.95 |          0.0010 |
|       1 |         500 |       00:00:44 |       87.50% |         0.89 |          0.0010 |
|       2 |         600 |       00:00:53 |      100.00% |         0.84 |          0.0010 |
|       2 |         700 |       00:01:02 |       87.50% |         0.83 |          0.0010 |
|       2 |         800 |       00:01:11 |       92.19% |         0.70 |          0.0010 |
|       2 |         900 |       00:01:20 |       98.44% |         0.78 |          0.0010 |
|       2 |        1000 |       00:01:29 |       75.00% |         0.68 |          0.0010 |
|       3 |        1100 |       00:01:37 |       79.69% |         1.01 |          0.0010 |
|       3 |        1200 |       00:01:46 |       84.38% |         1.37 |          0.0010 |
|       3 |        1300 |       00:01:55 |       87.50% |         1.17 |          0.0010 |
|       3 |        1400 |       00:02:04 |       96.83% |         0.84 |          0.0010 |
|       3 |        1500 |       00:02:13 |       73.44% |         0.96 |          0.0010 |
|       3 |        1600 |       00:02:22 |       85.94% |         1.19 |          0.0010 |
|       4 |        1700 |       00:02:31 |       98.44% |         0.71 |          0.0010 |
|       4 |        1800 |       00:02:40 |       95.31% |         1.04 |          0.0010 |
|       4 |        1900 |       00:02:49 |       73.44% |         0.98 |          0.0010 |
|       4 |        2000 |       00:02:58 |       95.31% |         0.92 |          0.0010 |
|       4 |        2100 |       00:03:06 |       95.31% |         0.88 |          0.0010 |
|       5 |        2200 |       00:03:15 |       90.63% |         0.81 |          0.0010 |
|       5 |        2300 |       00:03:24 |       95.31% |         0.64 |          0.0010 |
|       5 |        2400 |       00:03:33 |       96.88% |         0.86 |          0.0010 |
|       5 |        2500 |       00:03:42 |       90.63% |         1.18 |          0.0010 |
|       5 |        2600 |       00:03:51 |       79.69% |         1.08 |          0.0010 |
|       5 |        2700 |       00:04:00 |       96.88% |         0.92 |          0.0010 |
|       6 |        2800 |       00:04:09 |       90.63% |         0.77 |          0.0010 |
|       6 |        2900 |       00:04:18 |       95.31% |         0.69 |          0.0010 |
|       6 |        3000 |       00:04:27 |       90.63% |         1.04 |          0.0010 |
|       6 |        3100 |       00:04:36 |       92.06% |         0.87 |          0.0010 |
|       6 |        3200 |       00:04:45 |       92.19% |         0.75 |          0.0010 |
|       7 |        3300 |       00:04:54 |       89.06% |         0.69 |          0.0001 |
|       7 |        3400 |       00:05:03 |       96.88% |         0.77 |          0.0001 |
|       7 |        3500 |       00:05:12 |       85.94% |         1.05 |          0.0001 |
|       7 |        3600 |       00:05:21 |      100.00% |         0.66 |          0.0001 |
|       7 |        3700 |       00:05:29 |       93.75% |         0.73 |          0.0001 |
|       7 |        3800 |       00:05:38 |       93.65% |         0.69 |          0.0001 |
|       8 |        3900 |       00:05:47 |       89.06% |         0.68 |          0.0001 |
|       8 |        4000 |       00:05:56 |       95.31% |         0.86 |          0.0001 |
|       8 |        4100 |       00:06:05 |       98.44% |         0.93 |          0.0001 |
|       8 |        4200 |       00:06:14 |       93.75% |         0.85 |          0.0001 |
|       8 |        4300 |       00:06:23 |       96.88% |         0.58 |          0.0001 |
|       9 |        4400 |       00:06:32 |       96.88% |         0.68 |          0.0001 |
|       9 |        4500 |       00:06:41 |      100.00% |         0.63 |          0.0001 |
|       9 |        4600 |       00:06:50 |       96.88% |         0.64 |          0.0001 |
|       9 |        4700 |       00:06:59 |       96.88% |         0.89 |          0.0001 |
|       9 |        4800 |       00:07:08 |       96.88% |         0.61 |          0.0001 |
|       9 |        4900 |       00:07:17 |      100.00% |         0.53 |          0.0001 |
|      10 |        5000 |       00:07:26 |       85.94% |         0.59 |          0.0001 |
|      10 |        5100 |       00:07:35 |       93.75% |         1.04 |          0.0001 |
|      10 |        5200 |       00:07:44 |       89.06% |         0.66 |          0.0001 |
|      10 |        5300 |       00:07:53 |       96.88% |         0.49 |          0.0001 |
|      10 |        5400 |       00:08:01 |       96.88% |         0.79 |          0.0001 |
|      10 |        5470 |       00:08:08 |       87.50% |         0.77 |          0.0001 |
|========================================================================================|

Step 2 of 4: Training a Fast R-CNN Network using the RPN from step 1.
*******************************************************************
Training a Fast R-CNN Object Detector for the following object classes:

* Car
* Motorcycle
* Bus
* Truck

--> Extracting region proposals from 547 training images...done.

Training on single GPU.
|========================================================================================|
|  Epoch  |  Iteration  |  Time Elapsed  |  Mini-batch  |  Mini-batch  |  Base Learning  |
|         |             |   (hh:mm:ss)   |   Accuracy   |     RMSE     |      Rate       |
|========================================================================================|
|       1 |           1 |       00:00:00 |        9.38% |         0.48 |          0.0010 |
|       1 |         100 |       00:00:11 |       82.81% |         0.53 |          0.0010 |
|       1 |         200 |       00:00:23 |       89.06% |         0.59 |          0.0010 |
|       1 |         300 |       00:00:36 |       75.00% |         0.48 |          0.0010 |
|       1 |         400 |       00:00:48 |       81.25% |         0.47 |          0.0010 |
|       1 |         500 |       00:01:00 |       85.94% |         0.46 |          0.0010 |
|       2 |         600 |       00:01:12 |       82.81% |         0.47 |          0.0010 |
|       2 |         700 |       00:01:24 |       48.44% |         0.49 |          0.0010 |
|       2 |         800 |       00:01:36 |       82.81% |         0.50 |          0.0010 |
|       2 |         900 |       00:01:48 |       68.75% |         0.47 |          0.0010 |
|       2 |        1000 |       00:02:00 |       71.88% |         0.46 |          0.0010 |
|       3 |        1100 |       00:02:12 |       78.13% |         0.47 |          0.0010 |
|       3 |        1200 |       00:02:24 |       93.75% |         0.48 |          0.0010 |
|       3 |        1300 |       00:02:36 |       90.63% |         0.45 |          0.0010 |
|       3 |        1400 |       00:02:49 |       67.19% |         0.46 |          0.0010 |
|       3 |        1500 |       00:03:01 |       82.81% |         0.47 |          0.0010 |
|       3 |        1600 |       00:03:13 |       96.88% |         0.57 |          0.0010 |
|       4 |        1700 |       00:03:25 |       85.94% |         0.38 |          0.0010 |
|       4 |        1800 |       00:03:37 |       84.38% |         0.45 |          0.0010 |
|       4 |        1900 |       00:03:49 |       96.88% |         0.47 |          0.0010 |
|       4 |        2000 |       00:04:01 |       87.50% |         0.45 |          0.0010 |
|       4 |        2100 |       00:04:14 |       93.75% |         0.40 |          0.0010 |
|       5 |        2200 |       00:04:26 |       90.63% |         0.46 |          0.0010 |
|       5 |        2300 |       00:04:38 |       98.44% |         0.38 |          0.0010 |
|       5 |        2400 |       00:04:50 |       98.44% |         0.50 |          0.0010 |
|       5 |        2500 |       00:05:02 |       93.75% |         0.46 |          0.0010 |
|       5 |        2600 |       00:05:14 |       96.88% |         0.30 |          0.0010 |
|       5 |        2700 |       00:05:27 |       93.75% |         0.35 |          0.0010 |
|       6 |        2800 |       00:05:39 |       90.63% |         0.42 |          0.0010 |
|       6 |        2900 |       00:05:51 |       89.06% |         0.48 |          0.0010 |
|       6 |        3000 |       00:06:03 |       64.06% |         0.43 |          0.0010 |
|       6 |        3100 |       00:06:15 |       87.50% |         0.53 |          0.0010 |
|       6 |        3200 |       00:06:28 |       92.19% |         0.36 |          0.0010 |
|       7 |        3300 |       00:06:40 |       96.88% |         0.38 |          0.0001 |
|       7 |        3400 |       00:06:52 |       98.44% |         0.47 |          0.0001 |
|       7 |        3500 |       00:07:04 |       92.19% |         0.35 |          0.0001 |
|       7 |        3600 |       00:07:16 |       98.44% |         0.33 |          0.0001 |
|       7 |        3700 |       00:07:28 |       95.31% |         0.42 |          0.0001 |
|       7 |        3800 |       00:07:41 |       96.88% |         0.44 |          0.0001 |
|       8 |        3900 |       00:07:53 |       98.44% |         0.36 |          0.0001 |
|       8 |        4000 |       00:08:05 |       96.88% |         0.44 |          0.0001 |
|       8 |        4100 |       00:08:17 |       90.63% |         0.42 |          0.0001 |
|       8 |        4200 |       00:08:29 |       93.75% |         0.35 |          0.0001 |
|       8 |        4300 |       00:08:41 |       95.31% |         0.37 |          0.0001 |
|       9 |        4400 |       00:08:53 |       90.63% |         0.41 |          0.0001 |
|       9 |        4500 |       00:09:06 |       96.88% |         0.30 |          0.0001 |
|       9 |        4600 |       00:09:18 |      100.00% |         0.26 |          0.0001 |
|       9 |        4700 |       00:09:30 |       98.44% |         0.36 |          0.0001 |
|       9 |        4800 |       00:09:42 |       96.88% |         0.37 |          0.0001 |
|      10 |        4900 |       00:09:54 |       75.00% |         0.41 |          0.0001 |
|      10 |        5000 |       00:10:06 |       96.88% |         0.31 |          0.0001 |
|      10 |        5100 |       00:10:19 |      100.00% |         0.28 |          0.0001 |
|      10 |        5200 |       00:10:31 |      100.00% |         0.28 |          0.0001 |
|      10 |        5300 |       00:10:43 |       95.31% |         0.29 |          0.0001 |
|      10 |        5400 |       00:10:55 |       98.44% |         0.44 |          0.0001 |
|      10 |        5440 |       00:11:00 |       90.63% |         0.49 |          0.0001 |
|========================================================================================|

Step 3 of 4: Re-training RPN using weight sharing with Fast R-CNN.
Training on single GPU.
|========================================================================================|
|  Epoch  |  Iteration  |  Time Elapsed  |  Mini-batch  |  Mini-batch  |  Base Learning  |
|         |             |   (hh:mm:ss)   |   Accuracy   |     RMSE     |      Rate       |
|========================================================================================|
|       1 |           1 |       00:00:00 |       95.31% |         0.81 |          0.0010 |
|       1 |         100 |       00:00:05 |       87.50% |         0.81 |          0.0010 |
|       1 |         200 |       00:00:11 |       90.48% |         0.77 |          0.0010 |
|       1 |         300 |       00:00:16 |       92.19% |         0.82 |          0.0010 |
|       1 |         400 |       00:00:21 |       93.75% |         0.69 |          0.0010 |
|       1 |         500 |       00:00:27 |       96.88% |         0.60 |          0.0010 |
|       2 |         600 |       00:00:32 |       98.44% |         0.65 |          0.0010 |
|       2 |         700 |       00:00:38 |       90.63% |         0.84 |          0.0010 |
|       2 |         800 |       00:00:43 |       93.75% |         0.69 |          0.0010 |
|       2 |         900 |       00:00:48 |       92.19% |         0.65 |          0.0010 |
|       2 |        1000 |       00:00:54 |      100.00% |         0.90 |          0.0010 |
|       3 |        1100 |       00:00:59 |       98.44% |         0.72 |          0.0010 |
|       3 |        1200 |       00:01:05 |       96.88% |         0.72 |          0.0010 |
|       3 |        1300 |       00:01:10 |       95.31% |         1.38 |          0.0010 |
|       3 |        1400 |       00:01:15 |       96.88% |         0.89 |          0.0010 |
|       3 |        1500 |       00:01:21 |       95.31% |         0.59 |          0.0010 |
|       3 |        1600 |       00:01:26 |       92.19% |         0.69 |          0.0010 |
|       4 |        1700 |       00:01:32 |       96.88% |         0.62 |          0.0010 |
|       4 |        1800 |       00:01:37 |       98.44% |         0.79 |          0.0010 |
|       4 |        1900 |       00:01:42 |       85.94% |         1.39 |          0.0010 |
|       4 |        2000 |       00:01:47 |       92.19% |         0.60 |          0.0010 |
|       4 |        2100 |       00:01:53 |       96.88% |         0.81 |          0.0010 |
|       5 |        2200 |       00:01:58 |       96.88% |         1.37 |          0.0010 |
|       5 |        2300 |       00:02:04 |       98.44% |         0.91 |          0.0010 |
|       5 |        2400 |       00:02:09 |       95.31% |         0.45 |          0.0010 |
|       5 |        2500 |       00:02:15 |       96.88% |         0.43 |          0.0010 |
|       5 |        2600 |       00:02:20 |       87.30% |         0.89 |          0.0010 |
|       5 |        2700 |       00:02:25 |       92.19% |         0.73 |          0.0010 |
|       6 |        2800 |       00:02:30 |       96.88% |         0.54 |          0.0010 |
|       6 |        2900 |       00:02:35 |       90.63% |         0.84 |          0.0010 |
|       6 |        3000 |       00:02:41 |       96.88% |         0.94 |          0.0010 |
|       6 |        3100 |       00:02:46 |       96.88% |         0.70 |          0.0010 |
|       6 |        3200 |       00:02:51 |       92.19% |         0.70 |          0.0010 |
|       7 |        3300 |       00:02:56 |       98.41% |         0.88 |          0.0001 |
|       7 |        3400 |       00:03:02 |      100.00% |         0.88 |          0.0001 |
|       7 |        3500 |       00:03:07 |       95.31% |         0.68 |          0.0001 |
|       7 |        3600 |       00:03:13 |       98.44% |         0.56 |          0.0001 |
|       7 |        3700 |       00:03:18 |       96.83% |         0.57 |          0.0001 |
|       7 |        3800 |       00:03:23 |       93.75% |         0.64 |          0.0001 |
|       8 |        3900 |       00:03:29 |       96.88% |         0.61 |          0.0001 |
|       8 |        4000 |       00:03:34 |       98.44% |         0.82 |          0.0001 |
|       8 |        4100 |       00:03:39 |      100.00% |         0.71 |          0.0001 |
|       8 |        4200 |       00:03:45 |       96.83% |         0.59 |          0.0001 |
|       8 |        4300 |       00:03:50 |       90.63% |         0.66 |          0.0001 |
|       9 |        4400 |       00:03:55 |      100.00% |         0.61 |          0.0001 |
|       9 |        4500 |       00:04:00 |       96.88% |         0.58 |          0.0001 |
|       9 |        4600 |       00:04:06 |       95.31% |         0.63 |          0.0001 |
|       9 |        4700 |       00:04:11 |       95.31% |         0.52 |          0.0001 |
|       9 |        4800 |       00:04:16 |       96.88% |         0.93 |          0.0001 |
|       9 |        4900 |       00:04:22 |       98.44% |         0.71 |          0.0001 |
|      10 |        5000 |       00:04:27 |      100.00% |         0.56 |          0.0001 |
|      10 |        5100 |       00:04:33 |       96.88% |         0.39 |          0.0001 |
|      10 |        5200 |       00:04:38 |       95.24% |         0.50 |          0.0001 |
|      10 |        5300 |       00:04:44 |       93.75% |         1.09 |          0.0001 |
|      10 |        5400 |       00:04:49 |       93.75% |         0.56 |          0.0001 |
|      10 |        5470 |       00:04:53 |       96.88% |         0.65 |          0.0001 |
|========================================================================================|

Step 4 of 4: Re-training Fast R-CNN using updated RPN.
*******************************************************************
Training a Fast R-CNN Object Detector for the following object classes:

* Car
* Motorcycle
* Bus
* Truck

--> Extracting region proposals from 547 training images...done.

Training on single GPU.
|========================================================================================|
|  Epoch  |  Iteration  |  Time Elapsed  |  Mini-batch  |  Mini-batch  |  Base Learning  |
|         |             |   (hh:mm:ss)   |   Accuracy   |     RMSE     |      Rate       |
|========================================================================================|
|       1 |           1 |       00:00:00 |       98.44% |         0.21 |          0.0010 |
|       1 |         100 |       00:00:07 |       84.38% |         0.51 |          0.0010 |
|       1 |         200 |       00:00:15 |       87.50% |         0.35 |          0.0010 |
|       1 |         300 |       00:00:23 |       95.31% |         0.36 |          0.0010 |
|       1 |         400 |       00:00:31 |       93.75% |         0.48 |          0.0010 |
|       1 |         500 |       00:00:39 |       89.06% |         0.44 |          0.0010 |
|       2 |         600 |       00:00:47 |      100.00% |         0.28 |          0.0010 |
|       2 |         700 |       00:00:55 |       87.50% |         0.47 |          0.0010 |
|       2 |         800 |       00:01:03 |       98.44% |         0.27 |          0.0010 |
|       2 |         900 |       00:01:11 |      100.00% |         0.31 |          0.0010 |
|       2 |        1000 |       00:01:19 |      100.00% |         0.29 |          0.0010 |
|       3 |        1100 |       00:01:28 |       96.88% |         0.36 |          0.0010 |
|       3 |        1200 |       00:01:36 |       98.44% |         0.44 |          0.0010 |
|       3 |        1300 |       00:01:44 |       98.44% |         0.32 |          0.0010 |
|       3 |        1400 |       00:01:52 |      100.00% |         0.29 |          0.0010 |
|       3 |        1500 |       00:02:00 |       95.31% |         0.30 |          0.0010 |
|       3 |        1600 |       00:02:08 |       96.88% |         0.47 |          0.0010 |
|       4 |        1700 |       00:02:16 |      100.00% |         0.26 |          0.0010 |
|       4 |        1800 |       00:02:24 |      100.00% |         0.39 |          0.0010 |
|       4 |        1900 |       00:02:32 |       98.44% |         0.46 |          0.0010 |
|       4 |        2000 |       00:02:40 |       89.06% |         0.46 |          0.0010 |
|       4 |        2100 |       00:02:48 |       98.44% |         0.35 |          0.0010 |
|       5 |        2200 |       00:02:56 |      100.00% |         0.31 |          0.0010 |
|       5 |        2300 |       00:03:04 |       96.88% |         0.37 |          0.0010 |
|       5 |        2400 |       00:03:12 |      100.00% |         0.39 |          0.0010 |
|       5 |        2500 |       00:03:20 |       85.94% |         0.44 |          0.0010 |
|       5 |        2600 |       00:03:28 |       96.88% |         0.30 |          0.0010 |
|       5 |        2700 |       00:03:36 |       85.94% |         0.42 |          0.0010 |
|       6 |        2800 |       00:03:44 |       98.44% |         0.36 |          0.0010 |
|       6 |        2900 |       00:03:52 |       98.44% |         0.39 |          0.0010 |
|       6 |        3000 |       00:04:00 |       96.88% |         0.41 |          0.0010 |
|       6 |        3100 |       00:04:08 |       98.44% |         0.38 |          0.0010 |
|       6 |        3200 |       00:04:16 |       93.75% |         0.31 |          0.0010 |
|       7 |        3300 |       00:04:24 |       87.50% |         0.36 |          0.0001 |
|       7 |        3400 |       00:04:32 |      100.00% |         0.22 |          0.0001 |
|       7 |        3500 |       00:04:41 |       95.31% |         0.47 |          0.0001 |
|       7 |        3600 |       00:04:49 |       96.88% |         0.23 |          0.0001 |
|       7 |        3700 |       00:04:57 |      100.00% |         0.22 |          0.0001 |
|       7 |        3800 |       00:05:05 |      100.00% |         0.44 |          0.0001 |
|       8 |        3900 |       00:05:13 |       90.63% |         0.39 |          0.0001 |
|       8 |        4000 |       00:05:21 |       98.44% |         0.40 |          0.0001 |
|       8 |        4100 |       00:05:29 |       93.75% |         0.43 |          0.0001 |
|       8 |        4200 |       00:05:37 |      100.00% |         0.29 |          0.0001 |
|       8 |        4300 |       00:05:45 |      100.00% |         0.17 |          0.0001 |
|       9 |        4400 |       00:05:53 |       92.19% |         0.33 |          0.0001 |
|       9 |        4500 |       00:06:01 |       98.44% |         0.19 |          0.0001 |
|       9 |        4600 |       00:06:09 |       98.44% |         0.30 |          0.0001 |
|       9 |        4700 |       00:06:17 |       93.75% |         0.53 |          0.0001 |
|       9 |        4800 |       00:06:25 |      100.00% |         0.19 |          0.0001 |
|       9 |        4900 |       00:06:33 |       98.44% |         0.35 |          0.0001 |
|      10 |        5000 |       00:06:41 |       90.63% |         0.45 |          0.0001 |
|      10 |        5100 |       00:06:49 |       95.31% |         0.44 |          0.0001 |
|      10 |        5200 |       00:06:57 |       90.63% |         0.34 |          0.0001 |
|      10 |        5300 |       00:07:05 |       98.44% |         0.29 |          0.0001 |
|      10 |        5400 |       00:07:13 |       89.06% |         0.41 |          0.0001 |
|      10 |        5460 |       00:07:18 |      100.00% |         0.29 |          0.0001 |
|========================================================================================|

Finished training Faster R-CNN object detector.


detector = 

  <a href="matlab:helpPopup fasterRCNNObjectDetector" style="font-weight:bold">fasterRCNNObjectDetector</a> with properties:

                ModelName: 'Car'
                  Network: [1�1 vision.cnn.FastRCNN]
    RegionProposalNetwork: [1�1 vision.cnn.RegionProposalNetwork]
              MinBoxSizes: [21 26]
          BoxPyramidScale: 1.2000
      NumBoxPyramidLevels: 21
               ClassNames: {'Car'  'Motorcycle'  'Bus'  'Truck'  'Background'}
            MinObjectSize: [21 21]

Elapsed time is 2075.551570 seconds.
