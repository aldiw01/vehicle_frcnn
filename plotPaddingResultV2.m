load('results/resultAlex100.mat')
load('results/resultNP100.mat')
load('results/resultValid100.mat')
obj = ["Mobil" "Sepeda Motor" "Bus" "Truk"];
for n = 1:size(obj,2)
    f = figure;
    plot(cell2mat(resultAlex100.recall(n)),cell2mat(resultAlex100.precision(n)),...
        'LineStyle','--',...
        'Color','b',...
        'LineWidth',1)
    
    hold on
    plot(cell2mat(resultNP100.recall(n)),cell2mat(resultNP100.precision(n)),...
        'LineStyle','-',...
        'Color','r',...
        'LineWidth',1)
    
    hold on
    plot(cell2mat(resultValid100.recall(n)),cell2mat(resultValid100.precision(n)),...
        'LineStyle','-.',...
        'Color','k',...
        'LineWidth',1)
    
    xlabel('Recall')
    ylabel('Precision')
    grid on
%     axis([0 1 0 1])
    legend({'Alexnet','Same-Padding','Valid-Padding'},'Location','southeast');
    title(sprintf('Kurva Precision-Recall untuk %s',obj(n)))
    
    % Save Figure as pdf
%     set(f,'Units','Inches');
%     pos = get(f,'Position');
%     set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
%     print(f,sprintf('%s',obj(n)),'-dpdf','-r0')
end