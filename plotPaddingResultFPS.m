ep = [10 50 100];
x = [12.34 12.75 12.39];
y = [12.47 12.64 11.97];
z = [10.90 11.37 11.26];

f = figure;
plot(ep,x,'-o','MarkerIndices',1:length(x),...
    'LineStyle','--',...
    'Color','b',...
    'LineWidth',1.5)

hold on
plot(ep,y,'-d','MarkerIndices',1:length(x),...
    'LineStyle','-',...
    'Color','r',...
    'LineWidth',1.5)

hold on
plot(ep,z,'-v','MarkerIndices',1:length(x),...
    'LineStyle','-.',...
    'Color','k',...
    'LineWidth',1.5)

xlabel('Epoch')
ylabel('FPS')
grid on
% axis([10 100 10.5 13])
legend({'Alecnet','Same-Padding','Valid-Padding'},'Location','southeast');
% title(sprintf('Mean Average Precision Comparison'))

%% Save Figure as pdf
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(f,'mapResults','-dpdf','-r0')
