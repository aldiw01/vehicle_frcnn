color = 'mrgbk';
line = ["-" "--" ":" "-."];
mark = 'o+*.xsd^v><ph';
k=1;
f = figure;

x = [58301829 58301829 24747397];

c = categorical({'Alexnet','Same-Padding','Valid-Padding'});

bar(c,x);
ylim([0 6.5e7])
% set(gca,'YScale','log')
% plot(x,y,mark(mod(i,13)+1),'MarkerIndices',1:length(x),...
%     'LineStyle',line(mod(i,4)+1),...
%     'Color',color(mod(i,5)+1),...
%     'LineWidth',2)

text(1:length(x),x,num2str(x'),'vert','bottom','horiz','center'); 

% xlabel('Skema Padding')
ylabel('Learnable Parameter')
% axis([10 130 11.4 13])
% legend({sprintf('Alexnet [%d]',x(1)),sprintf('Same-Padding [%d]',x(1)),...
%     sprintf('Valid-Padding [%d]',x(1))},'Location','northeast');
% title(sprintf('Mean Average Precision Comparison'))

%% Save Figure as pdf
% set(f,'Units','Inches');
% pos = get(f,'Position');
% set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
% print(f,'ParameterPadding','-dpdf','-r0')

