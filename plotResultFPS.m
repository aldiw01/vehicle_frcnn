load('results\testResultsFilter5.mat')
color = 'mrgbk';
line = ["-" "--" ":" "-."];
mark = 'o+*.xsd^v><ph';
k=1;

for i=1:15
    for j=1:3
        x(j) = result.epoch(k);
        z(j) = result.fps(k);
        k=k+1;
    end
    
    leg = replace(string(cell2mat(result.filter(i*j))),'_','-');
    [x,idx] = sort(x);
    y = z(idx);
    
    f = figure;
    plot(x,y,mark(mod(i,13)+1),'MarkerIndices',1:length(x),...
        'LineStyle',line(mod(i,4)+1),...
        'Color',color(mod(i,5)+1),...
        'LineWidth',2)

    xlabel('Epoch')
    ylabel('FPS')
    axis([10 100 11.4 13])
    grid on
    legend(leg,'Location','southeast');
%     title(sprintf(leg))

    % Save Figure as pdf
    set(f,'Units','Inches');
    pos = get(f,'Position');
    set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
    print(f,sprintf(leg),'-dpdf','-r0')

end
