%% Load Dataset

disp('Loading Dataset ...');
t1 = load('dataset/testV1.mat');
t2 = load('dataset/testV2.mat');
t3 = load('dataset/testV3.mat');
t4 = load('dataset/testV4.mat');
t5 = load('dataset/testV5.mat');
t6 = load('dataset/testImg.mat');
% t = load('dataset/train.mat');

testDataset = [...
%     t1.gTruth
%     t2.gTruth
%     t3.gTruth
%     t4.gTruth
%     t5.gTruth
    t6.gTruth
%     t.gTruth
    ];
% t = load('dataset/train.mat');
% 
% testDataset = [t.gTruth];
%%
% Run detector on each image in the test set and collect results.

disp('Evaluating Network ...');
resultsStruct = struct([]);
for i = 1:height(testDataset)

    % Read the image.
    I = imread(testDataset.imageFilename{i});
%     I = imresize(I,2);
    % Run the detector.
    tic
    [bboxes, scores, labels] = detect(detector, I);
    time(i) = toc;
    % Collect the results.
    resultsStruct(i).Boxes = bboxes;
    resultsStruct(i).Scores = scores;
    resultsStruct(i).Labels = labels;
    if(mod(i,1000)==0)
        x = num2str([i height(testDataset) i/height(testDataset)*100])
    end
end

% Convert the results into a table.
results = struct2table(resultsStruct);
%% Extract expected bounding box locations from test data.
expectedResults = testDataset(:, 2:end);

% Evaluate the object detector using Average Precision metric.
[ap, recall, precision] = evaluateDetectionPrecision(results, expectedResults);
%% 
% The precision/recall (PR) curve highlights how precise a detector is at 
% varying levels of recall. Ideally, the precision would be 1 at all recall levels. 
% In this example, the average precision is ~0.6. The use of additional layers 
% in the network can help improve the average precision, but might require additional 
% training data and longer training time.
%%
% Plot precision/recall curve
for obj = 1:size(ap,1)
    figure
    plot(cell2mat(recall(obj)),cell2mat(precision(obj)),...
        'LineStyle',':',...
        'Color','r',...
        'LineWidth',1.5)
    xlabel('Recall')
    ylabel('Precision')
    grid on
    title(sprintf('Average Precision %s = %.1f', ...
        cell2mat(expectedResults.Properties.VariableNames(obj)), ap(obj)*100))
end
%%
% n=1;
% net(n) = "FRCNNPadd_11_5_3_3_3_Ep100_64_NP.mat";
% result2(n).net = net(n);
% result2(n).filter = extractBetween(net(n),"Padd_","_Ep");
% result2(n).epoch = str2double(extractBetween(net(n),"_Ep","_64"));
% result2(n).ap = ap(:);
% result2(n).map = mean(result2(n).ap);
% result2(n).npad = extractBetween(net(n),"_64",".mat");
% 
% result = struct2table(result2,'AsArray',true);
% save('results/testResultsPadding2.mat','result');