
f = figure;

x = [57908613 58301829 58301829];

c = categorical({'11-3-3-3-3','Full-training','Transfer Learning'});

bar(c,x);
ylim([5e7 6e7])
% set(gca,'YScale','log')
% plot(x,y,mark(mod(i,13)+1),'MarkerIndices',1:length(x),...
%     'LineStyle',line(mod(i,4)+1),...
%     'Color',color(mod(i,5)+1),...
%     'LineWidth',2)

text(1:length(x),x,num2str(x'),'vert','bottom','horiz','center'); 

% xlabel('Skema Filter')
ylabel('Learnable Parameter')
% axis([10 130 11.4 13])
% legend({'Hyperparameter'},'Location','northeast');
% title(sprintf('Mean Average Precision Comparison'))

%% Save Figure as pdf
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(f,'ParameterTrans','-dpdf','-r0')

