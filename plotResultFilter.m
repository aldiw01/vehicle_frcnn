load('results\testResultsFilter5.mat')
color = 'mrgbk';
line = ["-" "--" ":" "-."];
mark = 'o+*.xsd^v><ph';
k=1;
f = figure;

for i=1:15
    for j=1:3
        x(j) = result.epoch(k);
        z(j) = result.map(k)*100;
        k=k+1;
    end
    
    leg(i) = replace(string(cell2mat(result.filter(i*j))),'_','-');
    [x,idx] = sort(x);
    y = z(idx);
    plot(x,y,mark(mod(i,13)+1),'MarkerIndices',1:length(x),...
        'LineStyle',line(mod(i,4)+1),...
        'Color',color(mod(i,5)+1),...
        'LineWidth',2)
    
    hold on
end

xlabel('Epoch')
ylabel('mAP (%)')
% axis([10 130 11.4 13])
grid on
legend(leg,'Location','southeast');
% title(sprintf('Mean Average Precision Comparison'))

%% Save Figure as pdf
set(f,'Units','Inches');
pos = get(f,'Position');
set(f,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(f,'filterResults','-dpdf','-r0')
