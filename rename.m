close all;
clc;
y=90;
for i=110628:112911
    x = num2str(i);
    for j=1:8-length(x)
        x = ['0' x];
    end
    if(exist(sprintf('dataset/Test/Image/%s.jpg',x),'file'))
        img = imread(sprintf('dataset/Test/Image/%s.jpg',x));
        imwrite(img,sprintf('dataset/Test/Image/%d.jpg',y));
        delete(sprintf('dataset/Test/Image/%s.jpg',x));
        y=y+1;
    end
end