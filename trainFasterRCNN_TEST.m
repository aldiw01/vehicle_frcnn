%% Train Faster R-CNN Vehicle Detector

%% Preparing data.
load('dataset/train.mat');
gTruth.imageFilename = strcat(pwd,'\',gTruth.imageFilename);
trainingData = gTruth;

net = alexnet;

%% Configure training options.
epoch = 100;
for i = 1:length(epoch)
    diary(sprintf('logs/LogsFilt_TransLearn_Ep%d.txt', epoch(i)));
    trainOption = trainingOptions('sgdm', ...
        'MaxEpochs', epoch(i), ...
        'InitialLearnRate', 0.001, ...
        'MiniBatchSize',64, ...
        'LearnRateSchedule','piecewise', ...
        'LearnRateDropPeriod',round(epoch(i)*0.6), ...
        'LearnRateDropFactor',0.1, ...
        'VerboseFrequency', 100)

    %% Train detector.
    try
        tic
        detector = trainFasterRCNNObjectDetector(trainingData, net, trainOption, ...
                'NegativeOverlapRange', [0 0.4], ...
                'PositiveOverlapRange', [0.6 1], ...
                'BoxPyramidScale', 1.2)
        toc
        diary off;
        save(sprintf('results/filter/FRCNNFilt_TransLearn_Ep%d_%d.mat', ...
            epoch(i),trainOption.MiniBatchSize), 'detector');
    catch ME
%         system('shutdown -s -t 60');
        rethrow(ME)
        diary('Logs.txt');
    end
end
% system('shutdown -s -t 60');